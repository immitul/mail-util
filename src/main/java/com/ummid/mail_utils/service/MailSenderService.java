package com.ummid.mail_utils.service;

import com.ummid.mail_utils.model.Email;

public interface MailSenderService {
	void sendMail(Email email);
}
