package com.ummid.mail_utils.service.impl;

import com.ummid.mail_utils.model.Email;
import com.ummid.mail_utils.service.MailSenderService;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class MailSenderServiceImpl implements MailSenderService {

  static final Logger logger = LoggerFactory.getLogger(MailSenderServiceImpl.class);

  @Autowired
  private JavaMailSender javaMailSender;

  @Override
  @Async
  public void sendMail(Email email) {
    MimeMessage mimeMessage = javaMailSender.createMimeMessage();

    MimeMessageHelper mailMsg = new MimeMessageHelper(mimeMessage);
    try {
      mailMsg.setFrom(email.getFrom());
      mailMsg.setTo(email.getTo().toArray(new String[0]));
      mailMsg.setSubject(email.getSubject());
      mailMsg.setText(email.getMessage());

      if (CollectionUtils.isNotEmpty(email.getBcc())) {
        mailMsg.setBcc(email.getBcc().toArray(new String[0]));
      }

      if (CollectionUtils.isNotEmpty(email.getCc())) {
        mailMsg.setCc(email.getCc().toArray(new String[0]));
      }

    } catch (MessagingException e) {
      throw new RuntimeException("Error occured while creating mimemessage.", e);
    }

    javaMailSender.send(mimeMessage);
    logger.debug("Emai has been sent successfully");
  }
}
