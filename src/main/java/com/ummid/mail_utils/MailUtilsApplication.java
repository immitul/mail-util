package com.ummid.mail_utils;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@ComponentScan
@EnableAsync
public class MailUtilsApplication {

  public static void main(String[] args) {
    SpringApplication.run(MailUtilsApplication.class, args);
  }
}
